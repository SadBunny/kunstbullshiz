import './App.css';
import _ from 'lodash';
import { bullshit } from './bullshit.json'
import {Button, Paper} from "@material-ui/core";

import React, { useState } from 'react';

function refreshPage() {
    window.location.reload(false);
}

function App() {

    const [bullText, setBullText] = useState(generateBullText());
    const [imgUrl, setImgUrl] = useState(generateImgUrl());
    const [title, setTitle] = useState(generateTitle());

    function generateBullText() {
        return _.sample(bullshit.object) + " " + _.sample(bullshit.verb) + " " + _.sample(bullshit.subject) + " " + _.sample(bullshit.specification)
    };

    function generateImgUrl() {
        return _.sample(bullshit.images)
    }

    function generateTitle() {
        let a1 = _.sample(bullshit.titles.articles);
        let a2 = _.sample(bullshit.titles.articles);

        function gi(max) { return Math.floor(Math.random() * max) };

        function soms() { return gi(2) === 1 }

        let a1soms = true;
        let a2soms = true;
        // let a1soms = soms();
        // let a2soms = soms();

        let a1adj = _.sample(bullshit.titles.adjectives);
        let a2adj = _.sample(bullshit.titles.adjectives);

        let generation = (a1soms ? a1 : "" )
            + " " + (soms() ? a1adj : "" )
            + " " + _.sample(bullshit.titles.nouns[a1])
        ;
        if (soms()) {
            generation = generation + " " + _.sample(bullshit.titles.prepositions)
            + " " + (a2soms ? a2 : "")
            + " " + (soms() ? a2adj : "")
            + " " + _.sample(bullshit.titles.nouns[a2])
        }
        return generation.trim().replace(/\s{2,}/g,' ');
    }

    function generateArt() {
        setBullText(generateBullText());
        setImgUrl(generateImgUrl());
        setTitle(generateTitle());
    }

    return (
        <div className="App">
            <Art bullText={bullText} imgUrl={imgUrl} title={title}/>
            <p/>
            <div>
                <Button
                    variant="contained"
                    onClick={() => { generateArt() }}
                    style={{backgroundColor: '#cccccc', color: '#008800', fontWeight: '800'}}
                >
                    <i>Nóg een meesterwerk!</i>
                </Button>
            </div>
        </div>
    );
}

function Art(props) {

    const imgUrl = props.imgUrl;
    const bullText = props.bullText;
    const title = props.title;

    return (
        <div>
            <Paper style={{
                textTransform: 'capitalize',
                marginTop: '30px',
                marginLeft: 'auto',
                marginRight: 'auto',
                paddingTop: '10px',
                paddingBottom: '10px',
                maxWidth: '700px',
                backgroundColor: 'gold',
                color: 'darkblue',
                fontWeight: '800',
            }}>
                {title}
            </Paper>
            <div>
                <img src={imgUrl} style={{maxHeight: '250px', margin: "auto", marginTop: '20px'}}/>
            </div>
            <p/>
            <p/>
            <Paper elevation={5} style={{
                backgroundColor: '#BBBBBB',
                marginLeft: 'auto',
                marginRight: 'auto',
                maxWidth: '800px',
            }}>
                        <span style={{
                            display: "flex",
                            marginLeft: '10px',
                            marginRight: '10px',
                            color: "#EEEEEE",
                            backgroundColor: '#777777',
                        }}>
                            <div style={{fontSize: "30px", marginLeft: '5px'}}>&#8220; </div>
                            <div style={{
                                fontSize: "30px",
                                marginLeft: 'auto',
                                marginRight: 'auto'
                            }}>{bullText}.</div>
                            <div style={{fontSize: "30px", marginRight: '5px', marginTop: 'auto'}}> &#8221;</div>
                        </span>
            </Paper>
        </div>
    );
}

export default App;